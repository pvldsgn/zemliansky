import React from 'react'
import { Link } from 'react-router-dom'
import s from './index.module.sass'

// components
import Text from 'components/text'


const Notfound = () => {

    // Title
    React.useEffect(() => {
        document.title = "Pavel Zemliansky – 404"
    })

    // Events keyBoard
    React.useEffect(() => {
        const handleKey = (e) => {

            if (e.key == "Backspace") {
                window.history.back()
            }

            else if (e.key == "r" || e.key == 'R' || e.key == 'к' || e.key == 'К') {
                window.history.go(0)
            }
        }

        window.addEventListener('keydown', handleKey);
        return () => window.removeEventListener('keydown', handleKey);
    })

    return (
        <div className={s.page}>
            <Text className={s.title} size="h1">404</Text>
            <Link
                to="/"
                className={s.link}
            >
                <div className={s.back}>
                    <Text size="h3">Back to the future</Text>
                </div>
            </Link>
        </div>
    );
};

export default Notfound;