import React from 'react'
import s from './index.module.sass'

//components
import Text from 'components/text'
import Info from 'components/info'

// plugins
import Tilt from 'react-parallax-tilt'
import { Parallax, ParallaxLayer } from '@react-spring/parallax'
import { motion } from 'framer-motion'

// icons
import { ReactComponent as Dropdown } from 'icons/dropdown.svg'

const Platon = () => {

    const [dropdown, setDropdown] = React.useState(null)

    const toggle = (i) => {
        if (dropdown == i) {
            return setDropdown(null)
        }
        setDropdown(i)
    }

    // Title
    React.useEffect(() => {
        document.title = "Pavel Zemliansky – Platon"
    })

    // Events keyBoard
    React.useEffect(() => {
        const handleKey = (e) => {

            if (e.key == "Backspace") {
                window.history.back()
            }

            else if (e.key == "r" || e.key == 'R' || e.key == 'к' || e.key == 'К') {
                window.history.go(0)
            }
        }

        window.addEventListener('keydown', handleKey);
        return () => window.removeEventListener('keydown', handleKey);
    })

    return (
        <div style={{
            backgroundColor: 'linear-gradient(to top, #111F2A, #276C3C)',
            height: '100vh',
            transition: 'all 1s cubic-bezier(0.645, 0.045, 0.35, 1)'
        }}>
            <Info />
            <Parallax pages={5} scrolling vertical>

                {/* Title */}
                <section>
                    <Tilt
                        className={s.parallax}
                        perspective={1000}
                        tiltAxis='y'
                    >
                        <ParallaxLayer offset={0} speed={0.1}>
                            {/* scale screen */}
                            <motion.div
                                initial={{
                                    x: '-50%',
                                    y: '-50.2%',
                                    scale: 1
                                }}
                                animate={{
                                    x: '-50%',
                                    y: '-50.2%',
                                    scale: 1.15
                                }}
                                transition={{
                                    duration: 1.75
                                }}
                                className={s.wrapper}>
                                <video
                                    autoPlay
                                    loop
                                    playsInline
                                    preload='auto'
                                    className={s.img}
                                >
                                    <source
                                        src={"./img/platon.mp4"}
                                        type="video/mp4"
                                    />
                                </video>
                            </motion.div>
                        </ParallaxLayer>
                        <motion.div
                            initial={{
                                opacity: 1,
                                z: '-0px',
                                y: 0,
                                x: 0
                            }}
                            animate={{
                                opacity: .5,
                                z: '-200px',
                                y: 0,
                                x: 0
                            }}
                            transition={{
                                duration: 1.5
                            }}
                            className={s.name}
                        >
                            <Text size="h1">Platon</Text>
                        </motion.div>
                    </Tilt>
                </section>

                {/* Intro */}
                <section style={{ height: '90vh' }}>
                    <ParallaxLayer offset={0} speed={0.1} className={s.section}>
                        <div className={s.text}>
                            <div className={s.title}>
                                <Text size="h4">Intro</Text>
                            </div>

                            <Text size="h2">Platon</Text>
                        </div>
                    </ParallaxLayer>
                </section>

                {/* All-page's */}
                <section style={{ height: '50vh' }}>
                    <ParallaxLayer
                        offset={0}
                        style={{ height: '50vh', overflow: 'hidden' }}>
                        <ParallaxLayer offset={0} speed={0.2}>
                            <img src={"./img/platon/main.png"} className={s.img_all} alt="img" />
                        </ParallaxLayer>
                    </ParallaxLayer>
                </section>
                <section className={s.description}>
                    <Text size="h4">Initial process – MVP</Text>
                </section>

                {/* Problems */}
                <section>
                    <ParallaxLayer offset={0} speed={0.05} className={s.problems}>
                        <div className={s.title}>
                            <Text size="h4">Problems that were solved</Text>
                        </div>
                        <div className={s.accordion}>

                            {problems.map((item, i) => (
                                <div key={i} className={s.item} onClick={() => toggle(i)}>
                                    <div className={s.name}>
                                        <Text size="h2" className={s.problem_title}>{item.name}</Text>
                                        <Dropdown className={dropdown === i ? s.dropopen : s.dropdown} />
                                    </div>
                                    <div className={s.div} />
                                    <div className={dropdown === i ? s.answer_open : s.answer}>

                                        <div className={s.problem_info} style={{ opacity: .5 }}>
                                            <div className={s.descript}>
                                                <Text size="h4">Problem</Text>
                                            </div>
                                            <div className={s.problem_list}>
                                                <Text size="h3">{item.problem_1}</Text>
                                                <Text size="h3">{item.problem_2}</Text>
                                                <Text size="h3">{item.problem_3}</Text>
                                                <Text size="h3">{item.problem_4}</Text>
                                                <Text size="h3">{item.problem_5}</Text>
                                            </div>
                                        </div>

                                        <div className={s.div} />

                                        <div className={s.problem_info}>
                                            <div className={s.descript}>
                                                <Text size="h4">Solution</Text>
                                            </div>
                                            <Text size="h3">{item.solution}</Text>
                                        </div>

                                        <div className={s.div} />

                                        <div className={s.img_problem_wrapper}>
                                            <video
                                                autoPlay
                                                loop
                                                playsInline
                                                preload='auto'
                                                className={s.img_problem}
                                            >
                                                <source
                                                    src={item.imgProblem}
                                                    type="video/mp4"
                                                />
                                            </video>
                                        </div>
                                    </div>
                                </div>
                            ))}

                        </div>
                    </ParallaxLayer>
                </section>

            </Parallax>
        </div>
    );
};

const problems = [
    {
        name: 'Get news',
        problem_1: "❶ Employees often missed important news",
        problem_2: "❷ Did not participate in the life of the company",
        problem_3: "❸ Did not participate in surveys, proposals, were of little initiative.",
        problem_4: "❹ Some people did not know how and where this information is located.",
        problem_5: "❺ There was no way to save interesting news for yourself",
        solution: 'Make a general news stream, with the allocation of 5 important materials during the day, divide the rest of the news stream into a series with interruptions from polls, votes, and other useful features implemented in the application with the ability to add all of this to your personal favorite library',
        imgProblem: "./img/platon/platon.mp4"
    },
    {
        name: 'Phonebook',
        problem_1: "❶ It was difficult to find the employee's phone numbers if you didn't know his full name",
        problem_2: "❷ There was no way to find out about colleagues in neighboring departments",
        solution: 'Make a list of departments and divisions according to the dictionary with the ability to fall into it and find contact information of employees from certain or related departments'
    },
    {
        name: 'Birthdays',
        problem_1: "❶ Many employees did not know where to look at the dates of birth of their colleagues, which is why they could not congratulate them in a timely manner",
        problem_2: "❷ Or employees forgot to congratulate, even if they knew",
        solution: 'On the news feed page, display a general list of all employees celebrating birthdays with an internal calendar, with the ability to set a reminder for a date, add to the calendar or reminders by the application itself'
    },
    {
        name: 'Document management',
        problem_1: "❶ Employees often forgot about signing documents, about their timely submission to the personnel department",
        problem_2: "❷ There were difficulties with understanding the filling of documents in the processes of business trips/vacations and other",
        problem_3: "❸ There was no understanding of how the approval processes and so on are arranged",
        solution: 'Make template forms for filling out standard documents and sending them to the HR department remotely using an electronic signature. With the ability to track the process of approval or other cases of signing all documents in circulation'
    },
    {
        name: 'Booking meeting rooms',
        problem_1: "❶ Interaction with Microsoft calendar processes from the mail or through a personal account at the reception",
        solution: 'The ability to book a platon room with detailed information about the capacity, photos or tracking its occupancy for a certain period of time through the app'
    }
]

export default Platon;