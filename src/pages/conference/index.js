import React from 'react'
import s from './index.module.sass'

// components
import Text from 'components/text'
import Info from 'components/info'

// plugins
import Tilt from 'react-parallax-tilt'
import { Parallax, ParallaxLayer } from 'react-spring/renderprops-addons'
import { motion } from 'framer-motion'

// icons
import Accordion from 'components/accordion'

const Conference = ({ name, color }) => {

    // Заголовок страницы
    React.useEffect(() => {
        document.title = "Pavel Zemliansky – " + name
    })

    // Затемнение при заходе
    const [bg, setBg] = React.useState(false)

    React.useEffect(() => {
        setTimeout(() => {
            setBg(true)
        }, 100);
    })

    // Появление заголовка при скролле
    const [title, setTitle] = React.useState(false)
    const parallaxRef = React.useRef()

    const ref = React.useRef()

    React.useEffect(() => {
        if (!parallaxRef.current || !parallaxRef.current.container) return
        parallaxRef.current.container.onscroll = onScroll
    })

    const onScroll = () => {
        if (parallaxRef.current.current >= 500) {
            setTitle(true)
        } else setTitle(false)
    }

    React.useEffect(() => {
        console.log(parallaxRef)
        console.log(parallaxRef.current.container)
    })

    // Клавиатура
    React.useEffect(() => {
        const handleKey = (e) => {

            if (e.key == "Backspace") {
                window.history.back()
            }

            else if (e.key == "r" || e.key == 'R' || e.key == 'к' || e.key == 'К') {
                window.history.go(0)
            }
        }

        window.addEventListener('keydown', handleKey);
        return () => window.removeEventListener('keydown', handleKey);
    })

    // Динамический хедер
    const dynamicTitle = (
        <div style={{ position: 'absolute', zIndex: 1, margin: '0 5em' }}>
            <div className={s.navigation}>
                <div onClick={() => parallaxRef.current.scrollTo(0)} className={title ? s.title_project_active : s.title_project}>
                    <Text size="h3">{name}</Text>
                </div>
            </div>
        </div>
    )

    //
    /// Экраны ///
    //

    // Первый экран
    const main = (
        <section>
            <Tilt
                className={s.parallax}
                perspective={1000}
                tiltAxis='y'
            >
                <ParallaxLayer offset={0} speed={0.1}>
                    {/* scale screen */}
                    <motion.div
                        initial={{
                            x: '-50%',
                            y: '-50.2%',
                            scale: 1
                        }}
                        animate={{
                            x: '-50%',
                            y: '-50.2%',
                            scale: 1.15
                        }}
                        transition={{
                            duration: 1.75
                        }}
                        className={s.wrapper}>
                        <video
                            autoPlay
                            loop
                            playsInline
                            preload='auto'
                            className={s.img}
                        >
                            <source
                                src={"./img/conference.mp4"}
                                type="video/mp4"
                            />
                        </video>
                    </motion.div>
                </ParallaxLayer>
                <motion.div
                    initial={{
                        opacity: 1,
                        z: '-0px',
                        y: 0,
                        x: 0
                    }}
                    animate={{
                        opacity: .5,
                        z: '-200px',
                        y: 0,
                        x: 0
                    }}
                    transition={{
                        duration: 1.5
                    }}
                    className={s.name}
                >
                    <Text size="h1">{name}</Text>
                </motion.div>
            </Tilt>
        </section>
    )

    // Интро
    const intro = (
        <section style={{ height: '90vh' }}>
            <ParallaxLayer ref={ref} offset={0.01} speed={0.1} className={s.section}>
                <div className={s.text}>
                    <div className={s.title}>
                        <Text size="h4">Intro</Text>
                    </div>

                    <Text size="h2">TASS.Conference is a web service for viewing and recording relevant topics for the TASS News Agency in a live broadcast.</Text>
                </div>
            </ParallaxLayer>
        </section>
    )

    // Экран с изображением презентации
    const parallaxScreen = (
        <>
            <section style={{ height: '50vh' }}>
                <ParallaxLayer
                    offset={0}
                    style={{ height: '50vh', overflow: 'hidden' }}>
                    <ParallaxLayer offset={0} speed={0.2}>
                        <img src={"./img/conference/main.png"} className={s.img_all} alt="img" />
                    </ParallaxLayer>
                </ParallaxLayer>
            </section>
            <section className={s.description}>
                <Text size="h4">Initial process – MVP</Text>
            </section>
        </>
    )

    // Массив Проблем/Решений
    const solution = [
        {
            name: 'AD banners',
            problem_1: "❶ Hey hey",
            problem_2: "❷ Hallo",
            solution: 'Great news',
            imgProblem: "./img/tassovec/main.png"
        },
        {
            name: 'Phonebook',
            problem_1: "❶ It was difficult to find the employee's phone numbers if you didn't know his full name",
            problem_2: "❷ There was no way to find out about colleagues in neighboring departments",
            solution: 'Make a list of departments and divisions according to the dictionary with the ability to fall into it and find contact information of employees from certain or related departments'
        }
    ]

    // Проблемы/решения
    const problems = (
        <section>
            <ParallaxLayer offset={0} speed={0.05} className={s.problems}>
                <div className={s.title}>
                    <Text size="h4">Problems that were solved</Text>
                </div>
                <Accordion solution={solution} />
            </ParallaxLayer>
        </section>
    )

    // Рендер
    return (
        <div
            className={s.page}
            style={bg ? { backgroundColor: '#0062CC' } : { backgroundColor: color }}
        >
            {dynamicTitle}
            <Info />
            <Parallax pages={5} ref={parallaxRef} scrolling vertical>
                {main}
                {intro}
                {parallaxScreen}
                {problems}
            </Parallax>
        </div >
    );
};

export default Conference;