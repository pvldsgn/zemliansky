import React from 'react'
import s from './index.module.sass'

//components
import Text from 'components/text'
import Info from 'components/info'
import Accordion from 'components/accordion'

// plugins
import Tilt from 'react-parallax-tilt'
import { Parallax, ParallaxLayer } from '@react-spring/parallax'
import { motion } from 'framer-motion'


const Tassovec = ({ name, color }) => {

    // Заголовок страницы
    React.useEffect(() => {
        document.title = "Pavel Zemliansky – " + name
    })

    // Затемнение при заходе
    const [bg, setBg] = React.useState(false)

    React.useEffect(() => {
        setTimeout(() => {
            setBg(true)
        }, 100);
    })

    // Появление заголовка при скролле
    const [title, setTitle] = React.useState(false)
    const parallaxRef = React.useRef(null)

    const ref = React.useRef()

    React.useEffect(() => {
        if (!parallaxRef.current || !parallaxRef.current.container) return
        parallaxRef.current.container.onscroll = onScroll
    })

    const onScroll = () => {
        if (parallaxRef.current.current >= 500) {
            setTitle(true)
        } else setTitle(false)
    }

    React.useEffect(() => {
        console.log(parallaxRef)
        console.log(parallaxRef.current)
    })

    // Клавиатура
    React.useEffect(() => {
        const handleKey = (e) => {

            if (e.key == "Backspace") {
                window.history.back()
            }

            else if (e.key == "r" || e.key == 'R' || e.key == 'к' || e.key == 'К') {
                window.history.go(0)
            }
        }

        window.addEventListener('keydown', handleKey);
        return () => window.removeEventListener('keydown', handleKey);
    })

    // Динамический хедер
    const dynamicTitle = (
        <div style={{ position: 'absolute', zIndex: 1, margin: '0 5em' }}>
            <div className={s.navigation}>
                <div onClick={() => ref.current.scrollTo(0)} className={title ? s.title_project_active : s.title_project}>
                    <Text size="h3">{name}</Text>
                </div>
            </div>
        </div>
    )

    //
    /// Экраны ///
    //

    // Первый экран
    const main = (
        <section>
            <Tilt
                className={s.parallax}
                perspective={1000}
                tiltAxis='y'
            >
                <ParallaxLayer offset={0} speed={0.2}>
                    {/* scale screen */}
                    <motion.div
                        initial={{
                            x: '-50%',
                            y: '-50.2%',
                            scale: 1
                        }}
                        animate={{
                            x: '-50%',
                            y: '-50.2%',
                            scale: 1.15
                        }}
                        transition={{
                            duration: 1.75
                        }}
                        className={s.wrapper}>
                        <video
                            autoPlay
                            loop
                            playsInline
                            preload='auto'
                            className={s.img}
                        >
                            <source
                                src={"./img/tassovec/tassovec-2.mp4"}
                                type="video/mp4"
                            />
                        </video>
                    </motion.div>
                </ParallaxLayer>
                <motion.div
                    initial={{
                        opacity: 1,
                        z: '-0px',
                        y: 0,
                        x: 0
                    }}
                    animate={{
                        opacity: .5,
                        z: '-200px',
                        y: 0,
                        x: 0
                    }}
                    transition={{
                        duration: 1.5
                    }}
                    className={s.name}
                >
                    <Text size="h1">{name}</Text>
                </motion.div>
            </Tilt>
        </section>
    )

    // Интро
    const intro = (
        <section style={{ height: '30vh' }}>
            <ParallaxLayer offset={1} speed={0.2} className={s.section}>
                <div className={s.text}>
                    <Text size="h2">Tassovec — mobile app.<br /> <br /> Was developed for the internal interaction of TASS employees and the comfortable use of the company's internal products</Text>
                </div>
            </ParallaxLayer>
        </section>
    )

    // Экран с изображением презентации
    const parallaxScreen = (
        <>
            <section style={{ height: '50vh' }}>
                <ParallaxLayer offset={2}
                    style={{ height: '50vh', overflow: 'hidden' }}>
                    <ParallaxLayer offset={0} speed={0.2}>
                        <img src={"./img/tassovec/main.png"} className={s.img_all} alt="img" />
                    </ParallaxLayer>
                </ParallaxLayer>
            </section>
            <section className={s.description}>
                <Text size="h4">Initial process – MVP</Text>
            </section>
        </>
    )

    // Массив Проблем/Решений
    const solution = [
        {
            name: 'Get news',
            problem_1: "❶ Employees often missed important news",
            problem_2: "❷ Did not participate in the life of the company",
            problem_3: "❸ Did not participate in surveys, proposals, were of little initiative.",
            problem_4: "❹ Some people did not know how and where this information is located.",
            problem_5: "❺ There was no way to save interesting news for yourself",
            solution: 'Make a general news stream, with the allocation of 5 important materials during the day, divide the rest of the news stream into a series with interruptions from polls, votes, and other useful features implemented in the application with the ability to add all of this to your personal favorite library',
            imgProblem: "./img/tassovec/tassovec.mp4"
        },
        {
            name: 'Phonebook',
            problem_1: "❶ It was difficult to find the employee's phone numbers if you didn't know his full name",
            problem_2: "❷ There was no way to find out about colleagues in neighboring departments",
            solution: 'Make a list of departments and divisions according to the dictionary with the ability to fall into it and find contact information of employees from certain or related departments'
        },
        {
            name: 'Birthdays',
            problem_1: "❶ Many employees did not know where to look at the dates of birth of their colleagues, which is why they could not congratulate them in a timely manner",
            problem_2: "❷ Or employees forgot to congratulate, even if they knew",
            solution: 'On the news feed page, display a general list of all employees celebrating birthdays with an internal calendar, with the ability to set a reminder for a date, add to the calendar or reminders by the application itself'
        },
        {
            name: 'Document management',
            problem_1: "❶ Employees often forgot about signing documents, about their timely submission to the personnel department",
            problem_2: "❷ There were difficulties with understanding the filling of documents in the processes of business trips/vacations and other",
            problem_3: "❸ There was no understanding of how the approval processes and so on are arranged",
            solution: 'Make template forms for filling out standard documents and sending them to the HR department remotely using an electronic signature. With the ability to track the process of approval or other cases of signing all documents in circulation'
        },
        {
            name: 'Booking meeting rooms',
            problem_1: "❶ Interaction with Microsoft calendar processes from the mail or through a personal account at the reception",
            solution: 'The ability to book a conference room with detailed information about the capacity, photos or tracking its occupancy for a certain period of time through the app'
        }
    ]

    // Проблемы/решения
    const problems = (
        <section>
            <ParallaxLayer offset={3} speed={0.05} className={s.problems}>
                <Accordion solution={solution} />
            </ParallaxLayer>
        </section>
    )

    // Рендер
    return (
        <div
            className={s.page}
            style={bg ? { backgroundColor: '#005A9F' } : { backgroundColor: color }}
        >
            {dynamicTitle}
            <Info />
            <Parallax pages={5} ref={ref}>
                {main}
                <ParallaxLayer offset={1} speed={1} sticky={{ start: 1, end: 2 }} style={{ padding: '.5em 0', maxHeight: '0', zIndex: '-1', }}>
                    <div className={s.title}>
                        <Text size="h4">Intro</Text>
                    </div>
                </ParallaxLayer>
                {intro}
                {parallaxScreen}
                <ParallaxLayer sticky={{ start: 3, end: 5 }} style={{ padding: '.5em 0', maxHeight: '0', zIndex: '-1', }}>
                    <div className={s.title}>
                        <Text size="h4">Problems that were solved</Text>
                    </div>
                </ParallaxLayer>
                {problems}
            </Parallax>
        </div >
    );
};

export default Tassovec;


                // {/* Goals */}
                // <section style={{ height: '110vh' }}>
                //     <ParallaxLayer offset={0} speed={0} className={s.section}>
                //         <div className={s.title}>
                //             <Text size="h2">Goals</Text>
                //         </div>
                //         <div className={s.goals}>
                //             <div />
                //             <div style={{ textAlign: 'center' }}>
                //                 <img src="./img/tassovec/tassovec_goals.png" style={{ height: '70vh' }} alt="" />
                //             </div>
                //             <div className={s.goals_text}>
                //                 <Text size="h3">
                //                     The internal kitchen of TASS is a set of interaction processes that are separate from each other, and the purpose of the application was to create one common access panel for the convenience of their use by forming them in a single format, namely:
                //                 </Text>
                //             </div>
                //         </div>
                //     </ParallaxLayer>
                // </section>

                // {/* Visual Language */}
                // <section>
                //     <div className={s.title}>
                //         <Text size="h2">Visual language</Text>
                //     </div>
                //     <div style={{ height: '60vh' }}>
                //         <ParallaxLayer
                //             offset={0}
                //             style={{ height: '50vh', overflow: 'hidden', display: 'flex' }}>
                //             <ParallaxLayer style={{ zIndex: 1, height: '10em', justifyContent: 'center', display: 'flex' }} offset={0.9} speed={0.2}>
                //                 <div className={s.draw} />
                //             </ParallaxLayer>
                //             <ParallaxLayer offset={0.5} speed={0.2}>
                //                 <img src={"./img/tassovec/tass.png"} className={s.img_all} alt="img" />
                //             </ParallaxLayer>
                //         </ParallaxLayer>
                //         <ParallaxLayer offset={0.5} speed={0} className={s.tass_text}>
                //             <Text size="h3">The TASS building is a monolithic structure built in 1917</Text>
                //         </ParallaxLayer>
                //     </div>
                //     <div style={{ height: '5vh' }}>
                //         <ParallaxLayer
                //             offset={0}
                //             speed={0.075}
                //             className={s.tass_block}>
                //             <ParallaxLayer className={s.info_tass}>
                //                 <div className={s.card_tass}>
                //                     <img src={"./img/tassovec/tass_img.png"} className={s.img_tass} alt="img" />
                //                     <Text size="h4">The windows of the building are a rectangle with rounded corners</Text>
                //                 </div>
                //             </ParallaxLayer>
                //         </ParallaxLayer>
                //     </div>

                // </section>