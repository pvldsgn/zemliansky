import React from "react";
import s from "./index.module.sass";

//components
import Text from 'components/text'

// icons
import { ReactComponent as Dropdown } from 'icons/dropdown.svg'



const Accordion = ({ solution }) => {

    // dropdown - solution block
    const [dropdown, setDropdown] = React.useState(null)

    const toggle = (i) => {
        if (dropdown == i) {
            return setDropdown(null)
        }
        setDropdown(i)
    }

    return (
        <div className={s.accordion}>

            {solution.map((item, i) => (
                <div key={`${item}_${i}`} className={s.item} onClick={() => toggle(i)}>
                    <div className={s.name}>
                        <Text size="h2" className={s.problem_title}>{item.name}</Text>
                        <div className={s.border}>
                            <Dropdown className={dropdown === i ? s.dropopen : s.dropdown} />
                        </div>
                    </div>
                    <div className={s.div} />
                    <div className={dropdown === i ? s.answer_open : s.answer}>

                        <div className={s.problem_info} style={{ opacity: .5 }}>
                            <div className={s.descript}>
                                <Text size="h4">Problem</Text>
                            </div>
                            <div className={s.problem_list}>
                                <Text size="h3">{item.problem_1}</Text>
                                <Text size="h3">{item.problem_2}</Text>
                                <Text size="h3">{item.problem_3}</Text>
                                <Text size="h3">{item.problem_4}</Text>
                                <Text size="h3">{item.problem_5}</Text>
                            </div>
                        </div>

                        <div className={s.div} />

                        <div className={s.problem_info}>
                            <div className={s.descript}>
                                <Text size="h4">Solution</Text>
                            </div>
                            <Text size="h3">{item.solution}</Text>
                        </div>

                        <div className={s.div} />

                        <div className={s.img_problem_wrapper}>
                            <video
                                autoPlay
                                loop
                                playsInline
                                preload='auto'
                                className={s.img_problem}
                            >
                                <source
                                    src={item.imgProblem}
                                    type="video/mp4"
                                />
                            </video>
                        </div>
                    </div>
                </div>
            ))}

        </div>
    );
};

export default Accordion;