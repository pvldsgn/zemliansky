import s from './index.module.sass'

// routers
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

// components
import Header from 'components/header'
import Cards from 'components/cards'
import Cursor from 'components/cursor'
// pages
import Tassovec from 'pages/tassovec'
import Today from 'pages/today'
import Conference from 'pages/conference'
import Platon from 'pages/platon'
import NotFound from 'pages/notfound'

// plugins
import { CSSTransition } from 'react-transition-group'
import { AnimatePresence } from 'framer-motion'


const routes = [
    {
        path: '/',
        name: 'Home',
        Component: Cards
    },
    {
        path: '/tassovec',
        name: 'Tassovec',
        bgColor: '#0064B1',
        Component: Tassovec
    },
    {
        path: '/conference',
        name: 'Conference',
        bgColor: '#006EE5',
        Component: Conference
    },
    {
        path: '/today',
        name: 'Today',
        Component: Today
    },
    {
        path: '/platon',
        name: 'Platon',
        Component: Platon
    },
    // {
    //     path: '*',
    //     name: 'NotFound',
    //     Component: NotFound
    // }
]


const App = () => {

    return (
        <Router>
            <Cursor />
            <Route
                render={({ location }) => (

                    <AnimatePresence initial={true} >
                        <Switch location={location} key={location.pathname}>

                            <>
                                <div className={s.app}>
                                    <Header />
                                    {routes.map(({
                                        path,
                                        name,
                                        bgColor,
                                        Component }) => (

                                        <Route key={path} exact path={path}>
                                            {({ match }) => (

                                                <CSSTransition
                                                    in={match != null}
                                                    timeout={500}
                                                    classNames={s}
                                                    unmountOnExit
                                                >
                                                    <div className={s.page}>

                                                        <Component
                                                            name={name}
                                                            color={bgColor}
                                                        />

                                                    </div>
                                                </CSSTransition>

                                            )}
                                        </Route>

                                    ))}
                                </div>
                            </>

                        </Switch>
                    </AnimatePresence>

                )}
            />

        </Router>
    );
};

export default App;