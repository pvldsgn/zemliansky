import s from "./index.module.sass"

import { Link } from 'react-router-dom'

// icons
import { ReactComponent as IconLinkedin } from 'icons/social/linkedin.svg'
import { ReactComponent as IconTelegram } from 'icons/social/telegram.svg'
// import { ReactComponent as Burger } from 'icons/burger.svg'


const Header = ({ onMouseOut, onMouseOver }) => {

    return (
        <>
            <div className={s.border}
                onMouseOut={onMouseOut}
                onMouseOver={onMouseOver}
            >
                <Link
                    to="/"
                    className={s.logo}
                    style={{
                        backgroundImage: 'url(./img/logo.png)',
                    }}
                />
            </div>


            <div
                className={s.social}
                style={{
                    zIndex: 1,
                    position: 'fixed',
                    right: '1.5em',
                    top: '1.5em'
                }}
            >
                <a
                    target="_blank"
                    href="https://t.me/sampashas"
                    rel="noreferrer"
                    className={s.social_border}
                >
                    <IconTelegram className={s.social_icon} />
                </a>
                <a
                    target="_blank"
                    href="https://www.linkedin.com/in/sampashas/"
                    rel="noreferrer"
                    className={s.social_border}
                >
                    <IconLinkedin className={s.social_icon} />
                </a>
            </div>
        </>
    );
};

export default Header;