import React from 'react'
import s from './index.module.sass'

// components
import Text from 'components/text'
// plugins
import { motion } from 'framer-motion'

const Info = () => {
    return (
        <motion.div
            initial={{
                y: 200
            }}
            animate={{
                y: 0
            }}
            transition={{
                duration: 1.5,
                ease: [0.645, 0.045, 0.35, 1]
            }}
            className={s.info}
        >
            <div className={s.block}>
                <Text size="h4">Lead Designer</Text>
                <Text size="h4"> / Begin 2021</Text>
            </div>
            <div className={s.border}>
                <motion.div
                    initial={{
                        y: -6,
                        opacity: .5
                    }}
                    animate={{
                        y: 6,
                        opacity: 1
                    }}
                    transition={{
                        duration: 1,
                        repeat: Infinity,
                        repeatType: 'reverse',
                        ease: [0.645, 0.045, 0.35, 1]
                    }}
                    className={s.pin}
                />
            </div>
            <div className={s.right}>
                <Text size="h4"></Text>
            </div>
        </motion.div>
    );
};

export default Info;