import React from "react"
import s from "./index.module.sass"

//plugins
import classNames from "classnames/bind"
const cx = classNames.bind(s);

const Text = ({ children, size = "s", as, className }) => {
    const Tag = as || "span";

    return (
        <Tag
            className={cx(s.text, className, {
                h1: size === "h1",
                h2: size === "h2",
                h3: size === "h3",
                h4: size === "h4",
            })}
        >
            {children}
        </Tag>
    );
};

export default Text;
