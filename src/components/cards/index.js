import React from 'react'
import { useHistory } from "react-router-dom"
import s from './index.module.sass'

// components
import Card from 'components/cards/card'
import Text from 'components/text'

// plugins
import classnames from "classnames/bind"
import { CSSTransition, TransitionGroup, } from 'react-transition-group'
import { motion } from 'framer-motion'

// css
const cx = classnames.bind(s)

// cards
const cardsArray = [
    {
        name: 'Tassovec',
        date: "Begin'21 - Currently",
        img: './img/tassovec.mp4',
        link: '/tassovec',
        bgColor: '#0064B1',
        bgVideo: './img/tassovec/tassovecBG.mp4',
        isDesktop: false
    },
    {
        name: 'Conference',
        date: "End'19 — Begin'20",
        img: './img/conference.mp4',
        link: '/conference',
        bgColor: '#006EE5',
        bgVideo: './img/conference/conferenceBG.mp4',
        isDesktop: true
    }
]

// slider
export const Cards = () => {

    const [active, setActive] = React.useState(0)
    const [shifted, setShifted] = React.useState(false)
    const [last, setLast] = React.useState(false)

    // const [isMotion, setIsMotion] = React.useState(false)

    const history = useHistory();

    // Заголовок страницы
    React.useEffect(() => {
        document.title = "Pavel Zemliansky – Projects"
    })

    // Изменение цвета бг когда карточка активна
    React.useEffect(() => {
        document.body.style.background = cardsArray[active].bgColor
        if (!cardsArray)
            return null

    }, [active])

    // События нажатия клавиатуры
    React.useEffect(() => {
        const handleKey = (e) => {

            // вводим переменную пересчитывающюю общее количество карточек в массиве для переиспользования
            const allCards = cardsArray.length - 1

            if (e.key == "ArrowLeft") {
                // если активный слайд не равен 0 (1 элемент массива, active выводит строковое значение)
                if (active != 0) {
                    // setIsMotion(false)
                    setActive(active - 1)
                    // const timer = setTimeout(() => {
                    //     setIsMotion(true)
                    // }, 750);
                    // return () => clearTimeout(timer);
                }
                else {
                    // setIsMotion(false)
                    setActive(active + allCards)
                    // const timer = setTimeout(() => {
                    //     setIsMotion(true)
                    // }, 750);
                    // return () => clearTimeout(timer);
                }
            }

            else if (e.key == "ArrowRight") {
                // если активный слайд не равен последнему значению (тоже строковое значение числа по итогу)
                if (active != allCards) {
                    // setIsMotion(false)
                    setActive(active + 1)
                    // const timer = setTimeout(() => {
                    //     setIsMotion(true)
                    // }, 750);
                    // return () => clearTimeout(timer);
                }
                else {
                    // setIsMotion(false)
                    setActive(active - allCards)
                    // const timer = setTimeout(() => {
                    //     setIsMotion(true)
                    // }, 750);
                    // return () => clearTimeout(timer);
                }
            }

            else if (e.key == "Enter") {
                setShifted(true)
                // setTimeout(() => {
                //     setIsMotion(false)
                // }, 100);
                const timer = setTimeout(() => {
                    history.push(cardsArray[active].link)
                }, 250);
                return () => clearTimeout(timer);
            }

            else if (e.key == "r" || e.key == 'R' || e.key == 'к' || e.key == 'К') {
                history.go(0)
            }
        }

        window.addEventListener('keydown', handleKey);
        return () => window.removeEventListener('keydown', handleKey);
    })

    // Анимация разъезжания карточек при переключении
    // React.useEffect(() => {
    //     const timer = setTimeout(() => {
    //         setIsMotion(true)
    //     }, 1000);
    //     return () => clearTimeout(timer);
    // }, []);


    // bgColor
    const background = (
        <TransitionGroup className={s.coverContainer}>
            <CSSTransition
                key={active}
                timeout={400}
                classNames={s}
            >
                <>
                    <div
                        className={s.cover}
                        style={{
                            background: cardsArray[active].bgColor
                        }}>
                    </div>
                    <video
                        autoPlay
                        loop
                        playsInline
                        muted
                        preload='auto'
                        className={s.video}
                        style={shifted ? { opacity: 0, transition: 'all .5s ease-in-out' } : { opacity: 1, transition: 'all .5s ease-in-out' }}
                    >
                        <source src={cardsArray[active].bgVideo} type="video/mp4" />
                    </video>
                </>
            </CSSTransition>
        </TransitionGroup>
    )

    // Слайдер
    const cardsList = (
        <div
            className={s.cardsList}
            style={{ transform: `translateX(${active * -50}%)` }}

        >
            {
                cardsArray.map(({
                    name,
                    img,
                    link,
                    isDesktop
                },
                    i) => {

                    const isActive = i === active
                    const isPrev = i === active - 1 || active > i
                    const isNext = i === active + 1 || active < i
                    const isLast = i === cardsArray.length - 1
                    const isShifted = (shifted && isPrev) || (shifted && isNext)

                    // const isPrevMotion = i === active - 1
                    // const isNextMotion = i === active + 1
                    // const cardIsMotion = (isMotion && isPrevMotion) || (isMotion && isNextMotion)

                    // Events click
                    const handleClick = () => {
                        if (isActive) {
                            // setIsMotion(false)
                            setShifted(true)
                            const timer = setTimeout(() => {
                                history.push(link)
                            }, 750);
                            return () => clearTimeout(timer);
                        }
                        if (isPrev) {
                            // setIsMotion(false)
                            setActive(active - 1)
                            console.log(isLast)
                            // const timer = setTimeout(() => {
                            //     setIsMotion(true)
                            // }, 750);
                            // return () => clearTimeout(timer);
                        } else if (isNext) {
                            // setIsMotion(false)
                            setActive(active + 1)
                            console.log(isLast)
                            // const timer = setTimeout(() => {
                            //     setIsMotion(true)
                            // }, 750);
                            // return () => clearTimeout(timer);
                        }
                    }

                    // Events onDrag
                    const onDragEnd = (e) => {
                        if (isActive) {

                            // вводим переменную пересчитывающюю общее количество карточек в массиве для переиспользования
                            const allCards = cardsArray.length - 1

                            // если смещение курсора больше или = ширине окна/2
                            if (e.clientX >= window.innerWidth / 2)
                                // и если элемент массива не равен 1ому в списке
                                if (cardsArray[i] != cardsArray[0]) {
                                    // setIsMotion(false)
                                    setActive(active - 1)
                                    // const timer = setTimeout(() => {
                                    //     setIsMotion(true)
                                    // }, 750);
                                    // return () => clearTimeout(timer);
                                }
                                else setActive(active + allCards)

                            // если смещение курсора меньше или = ширине окна/2
                            if (e.clientX <= window.innerWidth / 2)
                                // и если элемент массива не равен последнему в списке
                                if (i != allCards) {
                                    // setIsMotion(false)
                                    setActive(active + 1)
                                    // const timer = setTimeout(() => {
                                    //     setIsMotion(true)
                                    // }, 750);
                                    // return () => clearTimeout(timer);
                                }
                                else setActive(active - allCards)
                        }
                    }

                    return (
                        <Card
                            key={`${name}_${i}`}
                            name={name}
                            img={img}
                            link={link}
                            {...{
                                i,
                                isActive,
                                isPrev,
                                isNext,
                                isLast,
                                handleClick,
                                onDragEnd,
                                isDesktop,
                                isShifted,
                                shifted
                                // cardIsMotion
                            }}
                        />
                    )
                })
            }
        </div >
    )

    // Пагинация
    const bullits = (
        <div style={shifted ? {
            transform: 'translate(25vw, 1em)',
            transition: 'all 1s cubic-bezier(0.645, 0.045, 0.355, 1)',
            opacity: 0
        } : { transform: 'translateX(25vw)' }}>
            <motion.div
                initial={{
                    y: 20,
                    opacity: 0
                }}
                animate={{
                    y: 0,
                    opacity: 1
                }}
                transition={{
                    duration: 1,
                    ease: 'easeInOut'
                }}
            >
                <div
                    className={s.bullits}
                    style={{
                        transform: `translateX(${active * -50}%)`
                    }}
                >
                    {
                        cardsArray.map((card, i) => {
                            return (
                                <Text
                                    key={`${card}_${i}`}
                                    onClick={() => setActive(i)}
                                    size="h4"
                                    className={cx(s.bullit,
                                        {
                                            active: i === active,
                                            left: i < active,
                                            right: i > active
                                        })}
                                >
                                    {cardsArray[i].date}
                                </Text>
                            )
                        })
                    }
                </div>
            </motion.div>

        </div>
    )

    // Рендер
    return (
        <section className={s.cards}>
            {cardsList}
            {bullits}
            {background}
        </section>
    );
};

export default Cards



// const isLast = i === active + (cardsArray.length - 1) || i === active + - (cardsArray.length - 1)