import s from "./index.module.sass"

// components
import Text from 'components/text'

// plugins
import classnames from "classnames/bind"
import { motion } from 'framer-motion'

// css
const cx = classnames.bind(s)


const Card = ({
    name,
    img,
    isActive,
    isPrev,
    isNext,
    isLast,
    handleClick,
    onDragEnd,
    isDesktop,
    isShifted,
    cardIsMotion
}) => {


    return (
        <motion.div
            drag='x'
            dragConstraints={{ left: 0, right: 0, top: 0, bottom: 0 }}
            dragDirectionLock
            dragElastic={0.1}
            dragMomentum={false}
            onDragEnd={onDragEnd}

            className={cx(s.cardContainer, {
                active: isActive,
                prev: isPrev,
                next: isNext,
                last: isLast,
                desktop: isDesktop,
                shifted: isShifted,
                motion: cardIsMotion
            })}>

            <div
                className={s.card}
                onClick={handleClick}
            >
                <motion.div className={s.imgAnimate}
                    initial={{
                        scale: .75,
                        opacity: 0
                    }}
                    animate={{
                        scale: 1,
                        opacity: 1
                    }}
                    transition={{
                        duration: 1,
                        ease: [0.645, 0.045, 0.355, 1]
                    }}
                >
                    <video
                        autoPlay
                        loop
                        playsInline
                        preload='auto'
                        className={s.img}
                    >
                        <source
                            src={img}
                            type="video/mp4"
                        />
                    </video>
                </motion.div>

                <div
                    className={s.title}
                >
                    <motion.div
                        initial={{
                            scale: .5,
                            opacity: 0
                        }}
                        animate={{
                            scale: 1,
                            opacity: 1
                        }}
                        transition={{
                            duration: 1,
                            delay: .25,
                            ease: [0.645, 0.045, 0.355, 1]
                        }}
                    >
                        <Text size="h1">{name}</Text>
                    </motion.div>
                </div>

            </div>
        </motion.div >
    );
};

export default Card;