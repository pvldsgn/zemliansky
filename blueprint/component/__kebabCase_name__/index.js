import React from "react";
import s from "./index.module.sass";
// import classNames from "classnames/bind"
// const cx = classNames.bind(s);

const {{ pascalCase name }} = () => {
    return (
        <div className={s.{{camelCase name}} }>

        </div>
    );
};

export default {{ pascalCase name }};